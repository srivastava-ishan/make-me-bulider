import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {routes} from './routes';
import {ErrorComponent} from './components/error';
import {HomeScreenComponent} from './components/home-screen';
import {HeaderComponent} from './components/header';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SearchBarComponent} from './components/search-bar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {QuickSetupComponent} from './components/quick-setup';
import {BrowseComponent} from './components/browse';
import {GetQuoteComponent} from './components/get-quote';
import {ContactUsComponent} from './components/contact-us';
import {LoginComponent} from './components/login';
import {DashboardComponent} from './components/dashboard';
import {NotFoundComponent} from './components/not-found';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    HomeScreenComponent,
    HeaderComponent,
    SearchBarComponent,
    QuickSetupComponent,
    BrowseComponent,
    GetQuoteComponent,
    ContactUsComponent,
    LoginComponent,
    DashboardComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(routes),
    FlexLayoutModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
