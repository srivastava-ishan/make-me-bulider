import {Routes} from '@angular/router';
import {HomeScreenComponent} from './components/home-screen';
import {BrowseComponent} from './components/browse';
import {GetQuoteComponent} from './components/get-quote';
import {ContactUsComponent} from './components/contact-us';
import {LoginComponent} from './components/login';
import {DashboardComponent} from './components/dashboard';
import {QuickSetupComponent} from './components/quick-setup';
import {NotFoundComponent} from './components/not-found';

export const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    children: [
      {
        path: '', component: HomeScreenComponent
      },
      {
        path: 'browse', component: BrowseComponent
      },
      {
        path: 'quick-setup', component: QuickSetupComponent
      },
      {
        path: 'get-quote', component: GetQuoteComponent
      },
      {
        path: 'contact', component: ContactUsComponent
      },
      {
        path: 'login', component: LoginComponent,
        // canActivate: [AuthGuard, OnboardingIncompleteGuard],
      }
    ],
  }, {
    path: '**', component: NotFoundComponent,
  }

];
