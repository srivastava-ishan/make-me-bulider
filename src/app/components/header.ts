import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'mmb-header',
  template: `
    <div class="major" style="height: 110px;padding-left:20px" fxLayout="row"
         fxLayoutAlign="start center">
      <img src="../../assets/images/mmb-logo-clr.png" style="height: 55px;cursor: pointer" fxFlexAlign="center" routerLink="">
      <span fxFlex="1 1 auto"></span>
      <div fxLayout="row" fxLayoutGap="10px" fxLayoutAlign="end center" style="margin-right: 5px" fxFlex="1 1 auto">
        <mmb-search-bar></mmb-search-bar>
        <button mat-button color="primary" routerLink="/quick-setup" routerLinkActive="selected">Quick Setup</button>
        <button mat-button color="primary" routerLink="/browse" routerLinkActive="selected">Browse</button>
        <button mat-button color="primary" routerLink="/get-quote" routerLinkActive="selected">Get Quote</button>
        <button mat-button color="primary" routerLink="/contact" routerLinkActive="selected">Contact Us</button>
        <button mat-button color="primary" routerLink="/login" routerLinkActive="selected">Login</button>
      </div>
    </div>`,
  styles: [`
    button.selected {
      border-bottom: 3px solid black;
    }

    .major {
      background: linear-gradient(132deg, #ec5218, #1665c1);
      background-size: 400% 400%;
      width: 100%;
      animation: BackgroundGradient 10s ease infinite;
    }

    @keyframes BackgroundGradient {
      0% {
        background-position: 0% 50%;
      }
      50% {
        background-position: 100% 50%;
      }
      100% {
        background-position: 0% 50%;
      }
    }
  `]
})

export class HeaderComponent {
  @Output() reload = new EventEmitter();
}
