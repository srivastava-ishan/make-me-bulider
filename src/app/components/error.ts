import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'mmb-error',
  template: `
    <h1>This is an 404! Try again later :(</h1>
  `,
  styles: [`
    span {
      font-size: x-large;
    }

    mat-icon {
      font-size: 60px;
      height: 60px;
      width: 60px;
    }
  `]
})

export class ErrorComponent {
  @Output() reload = new EventEmitter();
}
