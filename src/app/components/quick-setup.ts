import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'mmb-quick-setup',
  template: `
    <h1>The quick setup guide is still under construction.We'll let you know as soon as it is done.</h1>
  `,
  styles: [`
    span {
      font-size: x-large;
    }

    mat-icon {
      font-size: 60px;
      height: 60px;
      width: 60px;
    }
  `]
})

export class QuickSetupComponent {
  @Output() reload = new EventEmitter();
}
