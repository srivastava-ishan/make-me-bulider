import {Component} from '@angular/core';

@Component({
  selector: 'mmb-home-screen',
  template: `
    <img src="../../assets/images/bg1.jpeg" style="position: absolute;height:90%;width: 100%">
    <div style="width: 100%;height: 90%;position: relative" fxLayout="column"
         fxLayoutAlign="center start">
      <p style="margin-left: 40px;margin-bottom:-40px;margin-top:-80px;font-size:80px;color: antiquewhite">
        Make Me <span style="font-size: 80px;color: #ff7d04">Builder</span>
      </p>
      <div fxLayout="row" fxLayoutAlign="start center">
        <p style="margin-left: 40px;font-size:30px;color: antiquewhite">
          Let's make your place a &nbsp;
        </p>
        <p style="font-size:40px;border: solid thick #fff;" id="spin" color="primary">
        </p>
      </div>
    </div>`,
  styles: [`
    #spin {
      color: #ff4011;
    }

    #spin:after {
      content: "Family";
      animation: spin 8s linear infinite;
    }

    @keyframes spin {
      0% {
        content: "Abode";
         
      }
      10% {
        content: "Apartment";
         
      }
      20% {
        content: "Billet";
         
      }
      30% {
        content: "Boardinghouse";
         
      }
      40% {
        content: "Bungalow";
         
      }
      50% {
        content: "Cabin";
      }
      60% {
        content: "Casita";
      }
      70% {
        content: "Castle";
      }
      80% {
        content: "Chalet";
      }
      90% {
        content: "Chateau";
      }
    }
  `]
})

export class HomeScreenComponent {
}
