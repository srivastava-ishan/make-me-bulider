import {Component} from '@angular/core';

@Component({
  selector: 'mmb-dashboard',
  template: `
    <mmb-header></mmb-header>
    <router-outlet></router-outlet>`,
  styles: [`
  `]
})

export class DashboardComponent {
}
