import {Component} from '@angular/core';

@Component({
  selector: 'mmb-search-bar',
  template: `
    <div fxLayout="row" fxLayoutGap="5px" fxLayoutAlign="start center">
      <i class="material-icons">search</i>
      <mat-form-field>
        <input matInput>
      </mat-form-field>
    </div>

  `,
  styles: [`

  `]
})

export class SearchBarComponent {
}
