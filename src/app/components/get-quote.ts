import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'mmb-get-quote',
  template: `
    <h1>This page is still under construction.We'll let you know as soon as it is done.</h1>
  `,
  styles: [`
    span {
      font-size: x-large;
    }

    mat-icon {
      font-size: 60px;
      height: 60px;
      width: 60px;
    }
  `]
})

export class GetQuoteComponent {
  @Output() reload = new EventEmitter();
}
