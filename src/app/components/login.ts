import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'mmb-login',
  template: `
    <div class="overlay" fxLayoutAlign="center center">
      <div fxLayout="column" fxFlex="450px" fxFlex.xs="90%" fxLayoutGap="20px">
        <mat-card fxFlex="100%" style="background-color: #000000">
          <div fxLayout="row" fxLayoutGap="20px" fxLayoutAlign="start center">
            <h1>Login</h1>
            <span fxFlex="1 1 auto"></span>
            <img width="15%" height="15%" src="/assets/images/mmb-logo-clr.png">
          </div>
          <!--<form fxLayout="column" fxLayoutAlign="center stretch"-->
          <!--fxFlexAlign="center" fxLayoutGap="10px" novalidate>-->
          <mat-form-field style="width: 100%;">
            <input matInput placeholder="Email" formControlName="email">
            <mat-hint>Your business email</mat-hint>
            <mat-error>Valid email is required</mat-error>
          </mat-form-field>
          <mat-form-field style="width: 100%">
            <input matInput type="password" placeholder="Password" formControlName="password">
            <mat-error>Password is required</mat-error>
          </mat-form-field>
          <div fxLayout="row" fxLayoutAlign="end center">
            <button mat-raised-button fxFlexAlign="end" color="primary"
                    color="accent">
              Login
            </button>
          </div>
          <div fxLayout="row" fxLayoutAlign="center center" fxLayoutGap="10px" [style.margin-top]="'20px'">
            <a>Create New Account</a>
            <p>|</p>
            <a>Forgot Password?</a>
          </div>
          <!--</form>-->
        </mat-card>
      </div>
    </div>
  `,
  styles: [`
    .overlay {
      background: #ffdab4;
      background-image: -webkit-radial-gradient(top, circle cover, #ffdab4, #ffaa50 80%);
      background-image: -moz-radial-gradient(top, circle cover, #ffdab4, #ffaa50 80%);
      background-image: -o-radial-gradient(top, circle cover, #ffdab4, #ffaa50 80%);
      background-image: radial-gradient(top, circle cover, #ffdab4, #ffaa50 80%);
      height: 90%;
      width: 100%;
    }
  `]
})

export class LoginComponent {
}
